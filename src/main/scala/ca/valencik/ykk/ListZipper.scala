package ca.valencik.ykk

import cats._
import cats.implicits._

case class ListZipper[A](lefts: List[A], focus: A, rights: List[A]) {
  def leftMaybe: Option[ListZipper[A]] = this match {
    case ListZipper(Nil, f, rs)     => None
    case ListZipper(l :: ls, f, rs) => Some(ListZipper(ls, l, f :: rs))
  }
  def left: ListZipper[A] = leftMaybe.getOrElse(this)

  def rightMaybe: Option[ListZipper[A]] = this match {
    case ListZipper(ls, f, Nil)     => None
    case ListZipper(ls, f, r :: rs) => Some(ListZipper(f :: ls, r, rs))
  }
  def right: ListZipper[A] = rightMaybe.getOrElse(this)

  def lHead: A = lefts.headOption.getOrElse(focus)
  def rHead: A = rights.headOption.getOrElse(focus)

  def toList: List[A] = lefts.reverse ++ (focus :: rights)
  def zipZip[B](oz: ListZipper[B]): ListZipper[(A, B)] =
    ListZipper(lefts.zip(oz.lefts), (focus, oz.focus), rights.zip(oz.rights))
}
object ListZipper {
  def apply[A](xs: List[A], fi: Int): ListZipper[A] = {
    val (ls, rs) = xs.splitAt(fi)
    new ListZipper(ls.reverse, rs.head, rs.tail)
  }

  implicit def listZipperShow[A: Show]: Show[ListZipper[A]] =
    new Show[ListZipper[A]] {
      def show(z: ListZipper[A]): String = {
        val ls = z.lefts.reverse.mkString(",")
        val rs = z.rights.mkString(",")
        val f  = z.focus.show
        s"|$ls|> $f <|$rs|"
      }
    }

  implicit val catsInstancesForListZipper: Comonad[ListZipper] = new Comonad[ListZipper] {
    def map[A, B](za: ListZipper[A])(f: A => B): ListZipper[B] =
      ListZipper(za.lefts.map(f), f(za.focus), za.rights.map(f))

    def extract[A](z: ListZipper[A]): A = z.focus

    override def coflatten[A](za: ListZipper[A]): ListZipper[ListZipper[A]] = {
      val lefts: List[ListZipper[A]]  = List.iterate(za.left, za.lefts.size)(_.left)
      val rights: List[ListZipper[A]] = List.iterate(za.right, za.rights.size)(_.right)
      ListZipper(lefts, za, rights)
    }

    def coflatMap[A, B](za: ListZipper[A])(f: ListZipper[A] => B): ListZipper[B] = {
      coflatten(za).map(f)
    }
  }
}
