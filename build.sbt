lazy val catsVersion          = "1.4.0"
lazy val scalaTestVersion     = "3.0.5"
lazy val kindProjectorVersion = "0.9.8"

lazy val root = (project in file(".")).settings(
  organization := "ca.valencik",
  scalaVersion := "2.12.7",
  version := "0.1.0-SNAPSHOT",
  coverageMinimum := 75,
  scalacOptions ++= Seq(
    "-encoding",
    "UTF-8", // source files are in UTF-8
    "-deprecation", // warn about use of deprecated APIs
    "-unchecked", // warn about unchecked type parameters
    "-feature", // warn about misused language features
    "-language:higherKinds", // allow higher kinded types without `import scala.language.higherKinds`
    "-Xlint", // enable handy linter warnings
    "-Ypartial-unification" // allow the compiler to unify type constructors of different arities
  ),
  addCompilerPlugin("org.spire-math" %% "kind-projector" % kindProjectorVersion),
  scalacOptions in (Compile, console) --= Seq("-Xfatal-warnings", "-Xlint"),
  name := "YKK",
  libraryDependencies ++= Seq(
    "org.typelevel" %% "cats-core" % catsVersion,
    "org.scalatest" %% "scalatest" % scalaTestVersion % Test
  )
)
